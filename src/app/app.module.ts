import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { NavegationComponent } from './navegation/navegation.component';
import { MenuComponent } from './menu/menu.component';
import { MainComponent } from './main/main.component';
import { HorizontalComponent } from './main/navbar/horizontal/horizontal.component';


@NgModule({
  declarations: [
    AppComponent,
    NavegationComponent,
    MenuComponent,
    MainComponent,
    HorizontalComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
